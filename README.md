# Test Fullstack Muhammad Ilham Hidayat
Halo, selamat datang di project test fullstack Muhammad Ilham Hidayat. Pada project ini terdapat 2 aplikasi sekaligus yang dibalut melalui docker compose, yaitu **Aplikasi Landing Niagahoster** dan **Aplikasi Boxbilling**.

### Demo Aplikasi Landing Niagahoster
Berikut adalah link demo TEST - step 1 :
[https://test-fullstack.haamayat.com](http://test-fullstack.haamayat.com)

### Cara Install
Untuk menginstall aplikasi, pastikan telah terinstall aplikasi dibawah ini :

  - Composer
  - Docker Hub (Sudah termasuk Docker Engine)
  - Docker Compose

Jika semua aplikasi yang dibutuhkan telah terinstall, berikut adalah cara menginstall project test fullstack Muhammad Ilham Hidayat

Install vendor yang dibutuhkan pada **Root Folder** dimana terdapat **composer.json**

```sh
/root-folder$ composer install
```

Setelah vendor terinstall, lanjutkan deploy aplikasi ke localhost dengan **docker compose** di folder yang sama.

```sh
/root-folder$ docker-compose up -d
```

### Cara Akses
Setelah proses install selesai, masing - masing aplikasi dapat diakses melalui halaman :

| Aplikasi | Alamat |
| ------ | ------ |
| Aplikasi Landing Niagahoster | [http://localhost:80](http://localhost:80) |
| Aplikasi Boxbilling | [http://localhost:8004](http://localhost:8004) |

### Boxbilling Config
Teruntuk aplikasi boxbilling, telah disediakan akun demo baik **admin** atau pun **client** dengan detail akun berikut :

| User | Password | Link Login |
| ------ | ------ | ------ |
| admin@boxbilling.com (Admin) | demo | [http://localhost:8004/index.php?_url=/bb-admin/staff/login](http://localhost:8004/index.php?_url=/bb-admin/staff/login) |
| client@boxbilling.com (Client) | demo | [http://localhost:8004/index.php?_url=/login](http://localhost:8004/index.php?_url=/login) |

### Bila terjadi kendala, silahkan hubungi
- WA +6287824441970
- Email hamayat123@gmail.com
