FROM php:7.4.16-fpm-alpine3.13

RUN apk update \
    && apk add --no-cache git \
    && docker-php-ext-install mysqli pdo pdo_mysql \
    && docker-php-ext-enable pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html

RUN git clone https://github.com/boxbilling/boxbilling \
    && cd boxbilling/src \
    && composer install \
    && rm -rf /var/www/html/boxbilling/src/install

ADD ./docker-config/bb-config.php /var/www/html/boxbilling/src

RUN chmod -R 777 /var/www/html




