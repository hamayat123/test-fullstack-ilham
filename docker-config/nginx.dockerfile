FROM nginx:stable-alpine

ADD ./docker-config/nginx.conf /etc/nginx/nginx.conf

WORKDIR /var/www/html/
