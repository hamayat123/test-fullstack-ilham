$.getJSON( "assets/js/price-list.json", function( data ) {
    var container = $('#package-container');
    var templateClone = '';
    var packageHTML = '';

    data.forEach(function(v){
        templateClone = $('.package-template').clone();
        templateClone.find('.name').text(v.name);

        $.each(v, function(kAttr, vAttr){
            setDetail(templateClone, kAttr, vAttr)
        })

        var price = v.price.split('.');
        templateClone.find('.price').html(`<span>${price[0]}</span>.${price[1]}`);

        if(v.best_seller){
            templateClone.find('.package-item').addClass('best-seller')
        }

        if(v.discount_button){
            templateClone.find('.btn').text(`Diskon ${v.discount_button}%`);
        }

        if(v.best_seller) {
            templateClone.find('.package-item').prepend('<div class="ribbon"><span>BEST SELLER!</span></div>');
        }

        templateClone.removeClass('d-none');

        packageHTML += templateClone[0].outerHTML;
    })

    container.html(packageHTML);

    $('.package-template.d-none').remove();
});

function setDetail(templateClone, keyAttribute, valueAttribute) {
    if(valueAttribute === false){
        templateClone.find(`.${keyAttribute}`).parent().remove();
    }else{
        if(valueAttribute){
            templateClone.find(`.${keyAttribute}`).text(valueAttribute);
        }else{
            templateClone.find(`.${keyAttribute}`).parent().remove();
        }
    }
}
